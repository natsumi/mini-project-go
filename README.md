# Gobasic
***

## install GO

```
    # แต่ถ้าณยังไม่ได้ติดตั้ง Git :
    brew install git

    /* 
    หากคุณติดตั้ง homebrew คุณสามารถติดตั้ง Go 
    ได้อย่างง่ายดายด้วย 
    */
    brew install go
    
    # Clone โปรเจ็ค
    git clone <project_url>

    // set GOBIN
    export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
    export GOBIN=$(go env GOPATH)/bin

```

## Setup GO Project

#### ทำการแก้ไขไฟล์ .env เพื่อตั้งค่าโปรแกรม
```
    DB_HOST=db_host
    DB_PORT=db_port
    DB_USER=db_user
    DB_PASSWORD=db_password
    DB_NAME=db_name
    SERVER_PORT=server_port
```
#### คำสั่งต่างๆ
```go

    // build swagger เมื่อมีการ เพิ่มเส้น api หรือ แก้ไขเกี่ยวดับ swagger
    swag init service/dogservice.go swagger

    // คำสั่งในการ import 
    go get -u "url ที่ต้องการ import"
  
    //สำหรับการติดตั้ง ppackage ที่จำเป็น
    go mod tiny
    
    //
    swag init -g services/courseservice.go --output docs/
    
    // start project
    go run main.go  หรือ  go run .
    
    
```
