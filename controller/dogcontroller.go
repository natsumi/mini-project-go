package controller

import (
	"gobasic/service"

	_ "gobasic/docs"

	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
)

func SetRoutes(e *echo.Echo) {
	e.GET("/dog", service.GetDog)
	e.GET("/dog/:id", service.GetDogByID)
	e.GET("/dog/:spot", service.GetDogBySpot)
	e.POST("/dog", service.CreatcDog)
	e.PATCH("/dog/:id", service.UpdateDog)
	e.DELETE("/dog/:id", service.DelectDog)
	e.GET("/swagger/*", echoSwagger.WrapHandler)

}
