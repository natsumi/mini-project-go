package handler

import (
	// "database/sql"

	// "gorm.io/driver/sqlserver"
	"fmt"
	"os"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
}

func BuildDBConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     portConv(os.Getenv("DB_PORT")),
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		DBName:   os.Getenv("DB_NAME"),
	}
	fmt.Println("DB CONFIC:", dbConfig)
	return &dbConfig
}

// convert string to int
func portConv(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return i
}

func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
}

func DatabaseInit() {
	dbConfig := BuildDBConfig()
	dbURL := DbURL(dbConfig)
	fmt.Println("before connect db")
	var err error
	DB, err = gorm.Open(mysql.Open(dbURL), &gorm.Config{})

	fmt.Println("after connect db")
	if err != nil {
		panic("Failed to connect to database")
	}
}

func ConnectDB() *gorm.DB {
	return DB
}

// func ConnectDB() {

// 	db, err := sql.Open("mysql", "root:P@ssw0rd@tcp(127.0.0.1:3306)/only_cat")

// 	// if there is an error opening the connection, handle it
// 	if err != nil {
// 		panic(err.Error())
// 	}

// 	// defer the close till after the main function has finished
// 	// executing
// 	defer db.Close()

// 	// github.com/denisenkom/go-mssqldb
// 	dsn := "sqlserver://gorm:LoremIpsum86@localhost:9930?database=gorm"
// 	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})

// }
