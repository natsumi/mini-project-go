package main

import (
	"fmt"
	"gobasic/controller"
	"gobasic/handler"
	"gobasic/model"
	"log"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"github.com/joho/godotenv"
)

func main() {

	// Load environment variables from .env file
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}

	// Initialize Echo instance
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	// Set up Routes
	controller.SetRoutes(e)

	// / Connect to the database
	handler.DatabaseInit()
	defer handler.ConnectDB().DB()

	// Perform migrations using AutoMigrate
	db := handler.ConnectDB()
	fmt.Println("db :", db)
	fmt.Println("model :", &model.Dog{})
	err := db.AutoMigrate(&model.Dog{})
	if err != nil {
		log.Fatal(err)
	}

	// Start the server
	serverPort := os.Getenv("SERVER_PORT")
	fmt.Println("serverport :", serverPort)
	e.Logger.Fatal(e.Start(":" + serverPort))

}
