package model

type Dog struct {
	Id       int    `json:"id" gorm:"primaryKey"`
	Breed    string `json:"breed"`
	Hometown string `json:"hometown"`
	Spot     string `json:"spot"`
}

// returning table name
func (dog *Dog) TableName() string {
	return "dog"
}
