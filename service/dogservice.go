package service

import (
	"fmt"
	"net/http"

	"gobasic/handler"
	"gobasic/model"

	"github.com/labstack/echo/v4"
	"gopkg.in/go-playground/validator.v9"
)

//	@Summary		Show All Dog
//	@Description	get all Dog
//	@ID				get-string-by-int
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	model.Dog
//	@Failure		400	"Not found"
//	@Failure		404	"Not found"
//	@Header			200	{string}	Token	"qwerty"
//	@Router			/dog [get]
//
// Get All Dogs Data
func GetDog(c echo.Context) error {
	fmt.Println("before connect db")
	db := handler.ConnectDB()
	fmt.Println("after connect db")

	var dog []*model.Dog
	fmt.Print()
	fmt.Println("before if find")
	if err := db.Find(&dog); err.Error != nil {
		data := map[string]interface{}{
			"message": err.Error.Error(),
		}
		return c.JSON(http.StatusOK, data)
	}
	fmt.Println("after if find")
	response := map[string]interface{}{
		"message": "Fetch Successfully",
		"data":    dog,
	}

	return c.JSON(http.StatusOK, response)

}

//	@Summary		Show  Dog By ID
//	@Description	get  Dog By ID
//	@ID				get-string-by-id
//	@Accept			json
//	@Produce		json
//	@Param			id	path	int		    true	"Dog ID"
//	@Success		200	{object}	model.Dog
//	@Failure		400	"Not found"
//	@Failure		404	"Not found"
//	@Header			200	{string}	Token	"qwerty"
//	@Router			/dog/{id} [get]
//
// Get Dogs By ID
func GetDogByID(c echo.Context) error {

	db := handler.ConnectDB()

	id := c.Param("id")

	var dog []*model.Dog
	fmt.Print()

	if err := db.First(&dog, id); err.Error != nil {
		data := map[string]interface{}{
			"message": err.Error.Error(),
		}
		return c.JSON(http.StatusOK, data)
	}

	response := map[string]interface{}{
		"message": "Fetch Successfully",
		"data":    dog,
	}

	return c.JSON(http.StatusOK, response)

}

//	@Summary		Show  Dog By Spot
//	@Description	get  Dog By Spot
//	@ID				get-string-by-spot
//	@Accept			json
//	@Produce		json
//	@Param			spot	path	string		    true	"Dog Spot"
//	@Success		200	{object}	model.Dog
//	@Failure		400	"Not found"
//	@Failure		404	"Not found"
//	@Header			200	{string}	Token	"qwerty"
//	@Router			/dog/{spot} [get]
//
// Get Dogs By Spot
func GetDogBySpot(c echo.Context) error {

	db := handler.ConnectDB()

	spot := c.Param("spot")
	var dog []*model.Dog
	fmt.Print()

	if err := db.Find(&dog, "spot", spot); err.Error != nil {
		data := map[string]interface{}{
			"message": err.Error.Error(),
		}
		return c.JSON(http.StatusOK, data)
	}

	response := map[string]interface{}{
		"message": "Fetch Successfully",
		"data":    dog,
	}

	return c.JSON(http.StatusOK, response)

}

//	@Summary		Create Dog
//	@Description	Create Dog
//	@ID				creste-dog
//	@Accept			json
//	@Produce		json
//	@Param			dog	body		model.Dog	true	"Dog Data"
//	@Success		200		{object}	model.Dog
//	@Failure		400		"Not found"
//	@Failure		404		"Not found"
//	@Header			200		{string}	Token	"qwerty"
//	@Router			/dog [post]
//
// Create Dog Data
func CreatcDog(c echo.Context) error {

	db := handler.ConnectDB()

	dog := new(model.Dog)

	// Bind data
	if err := c.Bind(dog); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusBadRequest, data)
	}

	// Validate the dog struct
	validate := validator.New()

	if err := validate.Struct(dog); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusBadRequest, data)
	}

	if err := db.Create(&dog).Error; err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusInternalServerError, data)
	}

	response := map[string]interface{}{
		"message": "Create Successfully",
		"data":    dog,
	}
	return c.JSON(http.StatusOK, response)

}

//	@Summary		Update Dog
//	@Description	update Dog Object by ID
//	@ID				update-dog-by-id
//	@Accept			json
//	@Produce		json
//	@Param			id	path	int		    true	"Dog ID"
//	@Param			dog	body	model.Dog	true	"Dog Data"
//	@Success		200		{object}	model.Dog
//	@Failure		400		"Not found"
//	@Failure		404		"Not found"
//	@Header			200		{string}	Token	"qwerty"
//	@Router			/dog/{id} [PATCH]
//
// Update Dogs Data
func UpdateDog(c echo.Context) error {

	db := handler.ConnectDB()

	dog := new(model.Dog)

	// Bind data
	if err := c.Bind(dog); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusInternalServerError, data)
	}

	// Validate the dog struct
	validate := validator.New()
	if err := validate.Struct(dog); err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusBadRequest, data)
	}

	// Retrieve ID from the URL parameter
	id := c.Param("id")
	existing_dog := new(model.Dog)

	if err := db.First(&existing_dog, id).Error; err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusInternalServerError, data)
	}

	if dog.Breed != "" {
		existing_dog.Breed = dog.Breed
	}
	if dog.Hometown != "" {
		existing_dog.Hometown = dog.Hometown
	}
	if dog.Spot != "" {
		existing_dog.Spot = dog.Spot
	}

	if err := db.Save(&existing_dog).Error; err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusInternalServerError, data)
	}

	response := map[string]interface{}{
		"message": "Update Successfully",
		"data":    existing_dog,
	}
	return c.JSON(http.StatusOK, response)

}

//	@Summary		Delete Dog
//	@Description	delete Dog Object by ID
//	@ID				delete-dog-by-id
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Dog ID"
//	@Success		200	{object}	model.Dog
//	@Failure		400	"Not found"
//	@Failure		404	"Not found"
//	@Header			200	{string}	Token	"qwerty"
//	@Router			/dog/{id} [delete]
//
// Delete Dog Data
func DelectDog(c echo.Context) error {

	db := handler.ConnectDB()

	dog := new(model.Dog)

	id := c.Param("id")

	err := db.Delete(&dog, id).Error
	if err != nil {
		data := map[string]interface{}{
			"message": err.Error(),
		}
		return c.JSON(http.StatusInternalServerError, data)
	}

	response := map[string]interface{}{
		"message": "Delete Successfully!",
	}
	return c.JSON(http.StatusOK, response)

}
